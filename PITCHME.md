# Oops I did it again!

## Uma introdução ao git

### e mais especificamente sobre como corrigir coisas

![oops](https://2.bp.blogspot.com/-CBLM95HGvmE/WpQqe2QMSRI/AAAAAAAATQ0/zsZ0Z5aYXowYAPRfMu4z4csxP_67l6ByQCLcBGAs/s1600/again-2.gif)

---

## Fim do dia =)

```
> git status                                                                                             ✔  3623  08:11:20
On branch master
Your branch is up-to-date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   PITCHME.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.PITCHME.md.swp
	.babelrc
	.index.html.swp
	.my_new_file.js.swp
	app/
	config/
	img/
	index.html
	my_new_file.js
	node_modules/
	package.json
	scripts/
	yarn.lock

no changes added to commit (use "git add" and/or "git commit -a")
```

@[1]
@[2-3]
@[5-9]
@[11-26]
@[28]

---

## Cenário 1
### Dei `git add` sem querer, mas ainda não dei commit

---

## `git reset --help`

```
DESCRIPTION
       In the first and second form, copy entries from <tree-ish> to the index.
       In the third form, set the current branch head (HEAD) to <commit>,
       optionally modifying index and working tree to match. The
       <tree-ish>/<commit> defaults to HEAD in all forms.

       git reset [-q] [<tree-ish>] [--] <paths>...  This form resets the index
       entries for all <paths> to their state at <tree-ish>. (It does not
       affect the working tree or the current branch.)

           This means that git reset <paths> is the opposite of git add
           <paths>.
```

@[11-12]

---

Um caso muito comum é dar um `git add -A` e adicionar *tudo* ao staging.

Nesta situação você pode dar um `git reset` sem argumentos

Caso você queira cancelar a adição de um único arquivo: `git reset arquivo`

---

## Cenário 2
### Mas eu quero adicionar todos os arquivos `tracked`...

---

Neste caso, use `git add -u`

```
       -u, --update Update the index just where it already has an entry
       matching <pathspec>. This removes as well as modifies index entries to
       match the working tree, but adds no new files.

           If no <pathspec> is given when -u option is used, all tracked files
           in the entire working tree are updated (old versions of Git used to
           limit the update to the current directory and its subdirectories).
```

@[5-7]

---

## Cenário 3
### Tem um arquivo que toda hora eu dou `add` nele sem querer

---

## `.gitignore`

O Arquivo `.gitignore` faz o que o nome diz: o git ignora o arquivo.

Sintaxe dele é (quase) a sintaxe de glob * do Unix.

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.PITCHME.md.swp

$ echo "*.swp" >> .gitignore
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

(nada)
```

@[1,8]
@[10-13]

---

## Importante

Exemplo de arquivos/diretórios que (via de regra) não devem ir para o repositório:

- node\_modules
- bower\_components
- arquivos de configuração de IDE
- arquivos de swap do vim
- arquivos temporários que só você usa

---

## Cenário 4
### Dei um `commit` que não deveria, mas ainda não dei `push`

---

Neste caso basta fazer um `git reset HEAD~`

```
git commit -a -m "Adicionando arquivo errado"
git log --oneline --decorate

f627540 (HEAD -> master) Adicionando arquivo errado
a3c4b67 (origin/master) Adding sample project files
675de7d fixup! First slides, for testing gitpitch
dacf699 First slides, for testing gitpitch
b6736c9 New slideshow presentation

git reset HEAD~
git log --oneline --decorate

a3c4b67 (HEAD -> master, origin/master) Adding sample project files
675de7d fixup! First slides, for testing gitpitch
dacf699 First slides, for testing gitpitch
b6736c9 New slideshow presentation
```

@[4]
@[10,12-15]

---

## Pausa \#1
### Especificando revisões com `~` e `^`

![gad](https://i.stack.imgur.com/pDAzG.png)

---

## Cenário 5
### Dei `commit` em muitos arquivos, mas um, apenas, não deveria

---

Siga os mesmos passos do cenário 5, mas após o `git reset HEAD~` readicione e e dê commit apenas nos arquivos relevantes

---

## Pausa \#2
### Tipos de `git reset`

- `git reset --soft` disfaz o `commit` local e os arquivos continuam `staged`
- `git reset` disfaz o `commit` local e os arquivos continuam, mas `unstaged`
- `git reset --hard` *cuidado*: disfaz o `commit` local, e volta os arquivos\* para o estado original (`HEAD`)

---

## Cenário 6
### Esqueci de atualizar meu `branch` antes de dar `push` e o `git` não permitiu

---

É bem comum esquecer de dar um `git pull` de manhã, ou as vezes o colega deu um `push` no mesmo `branch antes de você`.

```
git push origin master

To gitlab.com:donatoaz/git-factalk.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'git@gitlab.com:donatoaz/git-factalk.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

@[6]

---

## Pausa \#3
### Diferença entre `tip`, `HEAD` e `head`

- `tip` é só um termo que se refere ao último `commit` em um `branch` nomeado.
- `head` é de fato uma refêrencia no git, ao `tip` do `branch` em questão
- `HEAD` se refere ao `commit` que se tem `checkout`
- por isso se diz `detached HEAD` e não `detached head`

---

```
$ git rev-parse --verify HEAD
8d6495c58c17b334714a4253dd498d7c2f0c527d
$ cat .git/refs/heads/concepta
8d6495c58c17b334714a4253dd498d7c2f0c527d
$ git checkout HEAD~~
$ git rev-parse --verify HEAD
42899cbbab9cbc9b24927fa7affba40cd008ed92
$ cat .git/refs/heads/concepta
8d6495c58c17b334714a4253dd498d7c2f0c527d
```

@[1-4]
@[2,4]
@[5,7,9]

---

## Cenário 7
### Meu colega alterou o mesmo trecho que eu, daí quando fiz o `git pull` deu conflito

---

```
git pull
Auto-merging app/index.js
CONFLICT (content): Merge conflict in app/index.js
Automatic merge failed; fix conflicts and then commit the result.
```

@[4]

---

Neste caso, temos de resolver o conflito. A melhor forma de resolução é a diplomacia.

Ou se você tem certeza de qual deve ser o comportamento do app, pode resolver sozinho.

Você pode descobrir quem fez a alteração perguntando a galera, ou usando o `git blame`.

```
00000000 (Not Committed Yet 2018-05-25 09:20:15 -0300 26) <<<<<<< HEAD
5de9d695 (Donato Azevedo    2018-05-25 09:15:34 -0300 27)       .then( imgData => res.send(`<img width="450px" src="${getImageURL(imgData)}"/>`) )
00000000 (Not Committed Yet 2018-05-25 09:20:15 -0300 28) =======
5394d8a2 (Colega            2018-05-25 09:10:08 -0300 29)       .then( imgData => res.send(`<img width="500px" src="${getImageURL(imgData)}"/>`) )
00000000 (Not Committed Yet 2018-05-25 09:20:15 -0300 30) >>>>>>> 5394d8a2016e8cbfa18f1633796b3b6db6f6de8f
```

@[4]

---

## Pausa \#4
### A pausa da chatisse

Cada um tem seu editor de texto, e como dev, deve assumir o compromisso de ser o mais produtivo possível com ele.

Não vou mostrar como resolver conflitos, pois depende do editor. Sugiro que corram atrás de aprender.

---

Resolvido o conflito, basta continuar o `commit`

---

## Cenário 8
### Alterei um monte de arquivos, mas tou no branch errado...

---

Se foi antes de dar o commit, basta seguir:

- `git stash` -- vai guardar suas alterações num cantinho
- `git checkout BRANCH_CERTO` -- vai mudar para o branch correto
- `git stash apply` -- vai trazer de volta do cantinho suas alterações

Se foi depois de dar o commit, siga o Cenário 5, repetir o acima.

---

## Cenário 9
### Criei um monte de arquivos sem querer e preciso removê-los

---

Imagine se você der `unzip arquivo_grande.zip` na raiz do seu projeto...

A solução é usar o `git clean`. Mas cuidado, ele realmente apaga coisas.

```
usage: git clean [-d] [-f] [-i] [-n] [-q] [-e <pattern>] [-x | -X] [--] <paths>...

    -q, --quiet           do not print names of files removed
    -n, --dry-run         dry run
    -f, --force           force
    -i, --interactive     interactive cleaning
    -d                    remove whole directories
    -e, --exclude <pattern>
                          add <pattern> to ignore rules
    -x                    remove ignored files, too
    -X                    remove only ignored files
```

---

## Cenário 9
### Dei commit *e push*, mas fiz 💩

#### E pode haver outras pessoas trabalhando neste mesmo `branch`

---

É um caso um pouco mais complexo, uma opção é simplesmente fazer outro `commit` revertendo o original.
E esta é a opção mais segura quando se está num projeto em que muitas pessoas estão trabalhando,
pois a reversão não desfaz parte história do projeto no remoto.

```
$ for x ({1..5}); echo "work"; echo "--beyonce"
$ git commit -a -m "Este commit vai ser revertido"
$ git push origin master
```

---

Oh oh, este último push tinha coisas erradas, mas muita gente já fez git pull nele e deu push no
mesmo repo...

```
git log --oneline --decorate

447201d Este commit vai ser revertido
3719cd7 More scenarios and pauses added
aa85f58 Merge branch 'master' of gitlab.com:donatoaz/git-factalk
63fa11a First version of factalk
```

@[3]

---

Hora de fazer um `git revert`: basta usar o `commit HASH` que se deseja reverter.

```
git revert 447201d
(escreve a mensagem de commit explicando porque está revertendo)
git log --oneline --decorate

2d51fcb (HEAD -> master, origin/master) Reverts "Este commit vai ser revertido"
447201d Este commit vai ser revertido
3719cd7 More scenarios and pauses added
aa85f58 Merge branch 'master' of gitlab.com:donatoaz/git-factalk
63fa11a First version of factalk
```

@[5]

---

## Cenário 9.2
### Dei commit *e push*, mas fiz 💩

#### Mas tenho certeza de que sou o único trabalhando neste branch

---

Outra opção é fazer um `reset` local e fazer um `git push --force` de volta para o remoto.

Atenção: normalmente o `branch` `master` *não* permite este tipo de ação -- é um branch protegido. O exemplo abaixo foi
feito num outro branch.

```
$ git log --oneline --decorate

a2b4143 (HEAD -> master, origin/master) Fazendo um commit que nao deveria
f3c5cb2 Accepting image size change of coleguinha
5de9d69 Change frog image size to 450px
```

---

```
$ git reset HEAD~
$ git checkout arquivos1 arquivo2 ...
$ git push --force origin feature/minha_feature
$ git log --oneline --decorate

f3c5cb2 (HEAD -> master) Accepting image size change of coleguinha
5de9d69 Change frog image size to 450px
```

---

## Cenário 10
### Acabei uma feature enorme, cheia de commits temporários e de pequenas correções

---

Em projetos de grande porte é normal se ter um pouco de TOC nos `branches` principais, evitando um monte de commits
com mensagens do tipo `esquecia vírgula` ou `fixes indentation`.

Nestes casos, antes de fazer o `merge` do `branch` do trabalho de volta no `branch` principal, os donos do projeto
exigem que os `merge requests` sejam `squashed` (comprimidos).

---

```
work...
  ~/src/facta/study/git-factalk     feature/nova-coisa  gaa && gcmsg "Adiciona subtítulo a pagina principal"
[feature/nova-coisa 56aecb5] Adiciona subtítulo a pagina principal
 1 file changed, 1 insertion(+), 1 deletion(-)
work...
  ~/src/facta/study/git-factalk     feature/nova-coisa  gaa && gcmsg "Esqueci da barra no h2"
[feature/nova-coisa ff23631] Esqueci da barra no h2
 1 file changed, 1 insertion(+), 1 deletion(-)
work...
  ~/src/facta/study/git-factalk     feature/nova-coisa  gaa && gcmsg "Corrigindo o título"
```

---

```
$ git log --oneline --decorate

01ac14a (HEAD -> feature/nova-coisa) Corrigindo o título
ff23631 Esqueci da barra no h2
56aecb5 Adiciona subtítulo a pagina principal
2d51fcb (origin/master, master) Reverts "Este commit vai ser revertido"
```

---

```
$ git rebase -i HEAD~3

 pick 56aecb5 Adiciona subtítulo a pagina principal
 fixup ff23631 Esqueci da barra no h2
 squash 01ac14a Corrigindo o título

 # Rebase 2d51fcb..01ac14a onto 2d51fcb (3 commands)
 #
 # Commands:
 # p, pick = use commit
 # r, reword = use commit, but edit the commit message
 # e, edit = use commit, but stop for amending
 # s, squash = use commit, but meld into previous commit
 # f, fixup = like "squash", but discard this commit's log message
 # x, exec = run command (the rest of the line) using shell
 # d, drop = remove commit
 #
 # These lines can be re-ordered; they are executed from top to bottom.
 #
 # If you remove a line here THAT COMMIT WILL BE LOST.
 #
 # However, if you remove everything, the rebase will be aborted.
 #
 # Note that empty commits are commented out
```

@[4,11]
@[5,15]
@[6,14]

---

```
# This is a combination of 3 commits.
# This is the 1st commit message:

Adiciona subtítulo a pagina principal

# The commit message #2 will be skipped:
# Esqueci da barra no h2

# This is the commit message #3:
Corrigindo o título

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# Date:      Sun May 27 12:11:08 2018 -0300
#
# interactive rebase in progress; onto 2d51fcb
# Last commands done (3 commands done):
#    fixup ff23631 Esqueci da barra no h2
#    squash 01ac14a Corrigindo o título
# No commands remaining.
# You are currently rebasing branch 'feature/nova-coisa' on '2d51fcb'.
#
# Changes to be committed:
#	modified:   app/index.js
```

@[1]
@[2,4]
@[6,7,9,10]

---

```
$ git log --oneline --decorate

0bc2e5d (HEAD -> feature/nova-coisa) Adiciona subtítulo a pagina principal
2d51fcb (origin/master, master) Reverts "Este commit vai ser revertido"
```

E após o merge no master

```
0bc2e5d (HEAD -> master, feature/nova-coisa) Adiciona subtítulo a pagina principal
2d51fcb (origin/master) Reverts "Este commit vai ser revertido"
```

## Cenário 11
### E se eu quiser manter a história, de que o `branch` saiu do `master` e voltou para o `master`?

---

Neste caso temos de fazer um merge sem `fast-forward`

```
work work, squash, open PR...
$ git merge --no-ff feature/nova-coisa2
$ git log --oneline --decorate --graph

*   1c9edf4 (HEAD -> master) Merge branch 'feature/nova-coisa2' with no ff
|\
| * 132175a (feature/nova-coisa2) Adds routes for cats and dogs
|/
* 0bc2e5d (feature/nova-coisa) Adiciona subtítulo a pagina principal
* 2d51fcb (origin/master) Reverts "Este commit vai ser revertido"
* 447201d Este commit vai ser revertido
```

@[5-8]

---

# Perguntas?
