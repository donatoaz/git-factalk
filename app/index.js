import config from 'config';

import http from 'http';
import express from 'express';
import { cat } from 'random-animal';
var randomFrog = require('random-frog');

let app = express();
let port = config.port || 8080;

/* add express middleware to app */
// app.use(bodyParser.urlencoded());    // bodyparser urlencoded
// app.use(bodyParser.json());          // bodyparser json
// app.use(morgan('dev'));              // morgan http-request-logger

const getImageURL = (imageData) => {
  if (!imageData) {
    return 'img/grumpy.gif'
  }
  return `https://imgur.com/${imageData.is_album ? imageData.album_cover : imageData.hash}${imageData.ext.replace(/\?.*/, '')}`
}

let router = express.Router();
router.get('/hello', (req, res) => {
    randomFrog()
      .then( imgData => res.send(`<img width="500px" src="${getImageURL(imgData)}"/>`) )
      .catch( e => res.send(`<h1>${e.message}</h1>`) )
});

router.get('/', (req, res) => { 
  res.send('<h1>Achou que não ia ter sapo?</h1><h2>Achou errado, otário!</h2>')  
});

router.get('/cats', (req, res) => { 
  res.send('<h1>Achou que não ia ter gato?</h1><h2>Achou certo, porque ainda não tem...</h2>')  
});

router.get('/dogs', (req, res) => { 
  res.send('<h1>Achou que não ia ter cachorro?</h1><h2>Achou certo, porque ainda não tem...</h2>')  
});

app.use(router);

let server = http.createServer(app);
server.listen(port, () => {
    console.log('API Server is listening on port:', port);
});
